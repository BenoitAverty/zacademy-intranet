package com.zenika.academy.backend.controllers;

import com.zenika.academy.backend.domain.Student;
import com.zenika.academy.backend.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class StudentsController {

    private StudentsRepository studentsRepository;

    @Autowired
    public StudentsController(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @GetMapping("/promotions/rennes-03/students")
    public List<String> getStudentsOfPromotion() {
        return this.studentsRepository.findAll().stream()
                .map(student -> student.getName())
                .collect(Collectors.toList());
    }
}
