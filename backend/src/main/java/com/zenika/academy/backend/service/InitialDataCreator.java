package com.zenika.academy.backend.service;

import com.zenika.academy.backend.domain.Student;
import com.zenika.academy.backend.repository.StudentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Component
public class InitialDataCreator {

    private StudentsRepository studentsRepository;

    @Autowired
    public InitialDataCreator(StudentsRepository studentsRepository) {
        this.studentsRepository = studentsRepository;
    }

    @PostConstruct
    public void createInitialData() {
        this.studentsRepository.save(new Student(UUID.randomUUID().toString(), "Norbert"));
        this.studentsRepository.save(new Student(UUID.randomUUID().toString(), "Jacquemine"));
    }
}
